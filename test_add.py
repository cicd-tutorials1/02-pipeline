import pytest

def add(a, b):
    return a + b

def test_foo():
    assert add(1, 2) == 3

def test_foo_fail():
    assert add(1, 2) == 5
